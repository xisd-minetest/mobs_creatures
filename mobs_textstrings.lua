--
-- This file register generic strings to be used in mob registration
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)


mobs.textstrings = {}
mobs.textstrings.longdesc = {}
mobs.textstrings.usagehelp = {}

--
-- Mobs type and habitat
--
local peacefull_animal = S("Peacefull animal")
local live_in_sky = S("flying in the sky")
local live_on_land = S("living on land")
local live_in_water = S("living in water")
local hostile_creature = S("Hostile creature")
local lurk_in_dark = S("lurking in the dark caves")
mobs.textstrings.longdesc.peacefull_sky = peacefull_animal..', '..live_in_sky..'.'
mobs.textstrings.longdesc.peacefull_land = peacefull_animal..', '..live_on_land..'.'
mobs.textstrings.longdesc.peacefull_water = peacefull_animal..', '..live_in_water..'.'
mobs.textstrings.longdesc.hostile_dark= hostile_creature..', '..lurk_in_dark..'.'
mobs.textstrings.longdesc.hostile_custom = function(where)
	return hostile_creature..', '..S("can be found @1.",where)
end
mobs.textstrings.longdesc.light_hurt = S("Is hurted by bright light.")


--
-- Mobs usage, feed, capture, ride
--

mobs.textstrings.usagehelp.feed_with = function(what)
	return S("Right-click with @1 to feed.",what)
end

function possible_to_catch(what)
	return S("it is possible to catch it using @1",what)
end
local net = S("a net")
local lasso = S("a lasso")
local net_or_lasso = net..' '..S("or")..' '..lasso
local when_tamed = S("When tamed")
local cannot_tame = S("Cannot be tamed but")
local even_not_tamed = S("even if not tamed")
local can_ride = S("can be ridden with a saddle")
mobs.textstrings.usagehelp.wild_catch_net = cannot_tame..', '..possible_to_catch(net)..'.'
mobs.textstrings.usagehelp.wild_catch_lasso = cannot_tame..', '..possible_to_catch(lasso)..'.'
mobs.textstrings.usagehelp.wild_catch_net_or_lasso = cannot_tame..', '..possible_to_catch(net_or_lasso)..'.'
mobs.textstrings.usagehelp.tamed_catch_net = when_tamed..', '..possible_to_catch(net)..'.'
mobs.textstrings.usagehelp.tamed_catch_lasso = when_tamed..', '..possible_to_catch(lasso)..'.'
mobs.textstrings.usagehelp.tamed_catch_net_or_lasso = when_tamed..', '..possible_to_catch(net_or_lasso)..'.'
-- TODO UPERCASE first letter
mobs.textstrings.usagehelp.wild_ride = can_ride..', '..even_not_tamed..'.'
mobs.textstrings.usagehelp.tamed_ride = when_tamed..', '..can_ride..'.'

--
-- Story, gameplay elements
--

mobs.textstrings.longdesc.a_long_time_ago_1 = S("A long time ago, this world was home of a great and thriving civilization.")
mobs.textstrings.longdesc.a_long_time_ago_2 = S("History is full of mysteries.")
mobs.textstrings.longdesc.a_long_time_ago_3 = S("The ancient world still echo into our present.")

mobs.textstrings.longdesc.history_zombie_1 = S("To this days, no one knows exactly what turned a wise people into roaming brainless monsters")
mobs.textstrings.longdesc.history_oerkki_1 = S("Knowing whether these shady creatures are a cause or a terrible consequence of the tragic falling of an ancient society remains a unanswered question.")

mobs.textstrings.longdesc.history_mummy_1 = S("Deads of the past times are haunting ancients tombs, and they are clearly not at rest.")
mobs.textstrings.longdesc.history_mummy_2 = S("These mummies walking around might be a good clue of it")


