--

 This is a bundle of mobs for mobs_redo
 Most of them are taken from various existing mods
  (Tell me if someone who should be credited isn't)

 The aim is to occasionally encounter mobs and be able to interact with them
	* No immediate threats on the surface : Player must be able to wander around and build things without being constantly attacked of annoyed by wildlife.
	* Choosed hack and slash situations : monsters can be a real challenge in some particular sitations (some areas, specific carried item...).
	* Minimalistic framework : Avoid adding a lot of craftitems and nodes that will lack proper integration and recipe
	* Maximal integration : compatibility with other mods, interaction betweed mobs, mobs and player, mobs and world
	*
**Warning** : This is still not finished and should be considered very unstable

--

-[x] Inventory image for all mobs
-[x] Settings    
    -[ ] Safe zone 
        * no monster inside a certain radius around spawn 
        * all mobs are passive and will run away inside a certain radius          
    -[ ] Progressive increment of mob damages and health (in a determined radius)
        * Mobs health and damages are proportionnal to distance from the orgin point until a defined max distance



-[ ] Alternative 'square pokeball' option for piched up mobs

-[-] Support for doc_identifier ( long description and usage )
	-[-] Proper long descriptions and usage help
		TODO : complete with wikipedia, reread
		TODO : slime description
	-[ ] Gameplay elements in textual description	

	-[ ] Traductions

-[ ] Special modes features
	-[ ] Trigger (node or something) for a tower defense mode
	-[ ] Nether integration with hostiles behaviors
	-[ ] Evil bunny trigger !!!!
	-[ ] Evil slime trigger !!!!

-[ ] Chat commands
	-[ ] Mesh, textures and sounds list for basic_robot users

-[ ] Treasurer support
-[ ] Updated lucky block support


-[ ] Useful drop or no new item
	-[ ] Slimeball drop
	-[ ] Lava flan drop orb ?? 

-[ ] Zombie spawners in dungeons
-[ ] Mummy spawners in pyramids

-[ ] Special features
	-[x] Rideable sheeps
	-[ ] Rideable dragons
	-[ ] Some "uniques" item
	-[ ] Potions to cure zombies
	-[ ] books


___________________________________________________
