
local S = mobs.intllib
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- mummy
	mobs:register_mob(modname..":mummy", {
		type = "monster",
		visual = "mesh",
		mesh = "mobs_sand_monster.b3d",
		textures = {
			{"creatures_mummy.png"},
		},
		collisionbox = {-0.25, -1, -0.3, 0.25, 0.75, 0.3},
		animation = {
			speed_normal = 10,		speed_run = 15,
			stand_start = 0,		stand_end = 79,
			walk_start = 168,		walk_end = 188,
			run_start = 168,		run_end = 188
		},
		makes_footstep_sound = true,
		sounds = {
			random = "creatures_mummy",
			war_cry = "mobs_zombie.3",
			attack = "mobs_zombie.2",
			damage = "creatures_mummy_hit",
			death = "creatures_mummy_death",
		},
		hp_min = 12, -- 12
		hp_max = 15, -- 35
		armor = 200,
		knock_back = 1,
		lava_damage = 10,
		damage = 4,
		reach = 2,
		attack_type = "dogfight",
		group_attack = true,
		view_range = 10,
		walk_chance = 75,
		walk_velocity = 0.5,
		run_velocity = 0.7,
		jump = false,
		runaway = true,
		drops = {
			{name = modname..":rotten_flesh", chance = 2, min = 1, max = 2,},
			{name = "default:papyrus", chance = 2, min = 1, max = 3,},
		},
		lifetimer = 180,		-- 3 minutes
		shoot_interval = 135,	-- (lifetimer - (lifetimer / 4)), borrowed for do_custom timer
	})

	--name, nodes, neighbors, min_light, max_light, interval, chance, active_object_count, min_height, max_height
	mobs:spawn_specific(modname..":zombie",
		{"default:sand", "default:desert_sand"},
		{"air"},
		-1, 5, 30, 20000, 2, -31000, 31000
	)
	mobs:register_egg(modname..":mummy", "Mummy", "creatures_mummy.png", 1)
