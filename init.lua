
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)


-- Load support for intllib.
local S, NS = dofile(modpath.."/intllib.lua")
mobs.intllib = S
mobs.textstrings = {}
mobs.textstrings.longdesc = {}
mobs.textstrings.usagehelp = {}


-- General functions, items, nodes
dofile(modpath .. "/mobs_textstrings.lua")
dofile(modpath .. "/mobs_api.lua")
dofile(modpath .. "/mobs_craftitems.lua")


--~ do return end

--
-- Animals
--

dofile(modpath .. "/mobs/animal_birds.lua")
dofile(modpath .. "/mobs/animal_bunny.lua")
dofile(modpath .. "/mobs/animal_chicken.lua")
dofile(modpath .. "/mobs/animal_fish.lua")
--dofile(modpath .. "/mobs/animal_flying_pig.lua")
dofile(modpath .. "/mobs/animal_jellyfish.lua")
dofile(modpath .. "/mobs/animal_kitten.lua")
dofile(modpath .. "/mobs/animal_penguin.lua")
dofile(modpath .. "/mobs/animal_sheep.lua")
dofile(modpath .. "/mobs/animal_turtles.lua")

--
-- Monsters
--

dofile(modpath .. "/mobs/monster_slimes.lua")
dofile(modpath .. "/mobs/monster_zombies.lua")
dofile(modpath .. "/mobs/monster_sand_monster.lua")
dofile(modpath .. "/mobs/monster_oerkki.lua")
dofile(modpath .. "/mobs/monster_lava_flan.lua")
--dofile(modpath .. "/mobs/dragon.lua")

--
-- NPCs
--


--
-- Map generation
--


dofile(modpath .. "/mobs_mapgen.lua")



--
-- Compatibility
--


dofile(modpath .. "/lucky_block.lua")

--
-- Notification
--

print ("[MOD] Mobs Redo 'mobs_bundle' loaded")
