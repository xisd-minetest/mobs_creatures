--
-- This file register nodes and items needed by this mod
--

local S = mobs.intllib
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Chicken
--
local CHICKEN_MEAT = minetest.settings:get_bool("mobs.animal_chicken.enable_chicken_meat") or false

-- Override existing mobs chicken egg
minetest.register_node(":mobs:egg", {
	description = S("Chicken Egg"),
	tiles = {"mobs_chicken_egg.png"},
	inventory_image  = "mobs_chicken_egg.png",
	visual_scale = 0.7,
	drawtype = "plantlike",
	wield_image = "mobs_chicken_egg.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	sunlight_propagates = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.2, -0.5, -0.2, 0.2, 0, 0.2}
	},
	groups = {snappy = 2, dig_immediate = 3},
	after_place_node = function(pos, placer, itemstack)
		if placer:is_player() then
			minetest.set_node(pos, {name = "mobs:egg", param2 = 1})
		end
	end,
	on_use = mobs.throw_egg
})

-- fried egg
minetest.register_craftitem(":mobs:chicken_egg_fried", {
	description = S("Fried Egg"),
	inventory_image = "mobs_chicken_egg_fried.png",
	on_use = minetest.item_eat(2),
})

minetest.register_craft({
	type  =  "cooking",
	recipe  = "mobs:egg",
	output = "mobs:chicken_egg_fried",
})


if CHICKEN_MEAT then
	-- raw chicken
	minetest.register_craftitem(":mobs:chicken_raw", {
	description = S("Raw Chicken"),
		inventory_image = "mobs_chicken_raw.png",
		on_use = minetest.item_eat(2),
	})

	-- cooked chicken
	minetest.register_craftitem(":mobs:chicken_cooked", {
	description = S("Cooked Chicken"),
		inventory_image = "mobs_chicken_cooked.png",
		on_use = minetest.item_eat(6),
	})

	minetest.register_craft({
		type  =  "cooking",
		recipe  = "mobs:chicken_raw",
		output = "mobs:chicken_cooked",
	})
end 

-- feather
minetest.register_craftitem(":mobs:feather", {
	description = S("Feather"),
	inventory_image = "mobs_feather.png",
})

--
--	Zombie
--

-- rotten flesh
	minetest.register_craftitem(":mobs:rotten_flesh", {
		description = "Rotten Flesh",
		inventory_image = S("mobs_rotten_flesh.png"),
		on_use = minetest.item_eat(1),
	})

--
-- Slimes
--
-- crafts
minetest.register_craftitem(":mobs:green_slimeball", {
	image = "mobs_green_slime_ball.png",
	description=S("Green Slime Goo"),
})
