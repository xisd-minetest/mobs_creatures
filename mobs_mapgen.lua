--
-- This file adds elements for the map generation
--

local S = mobs.intllib
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Place spawners in dungeons
--
local mapgen_elements = {
	dungeon = 	{"mobs:spawner_zombie"},
	temple 	= 	{"mobs:spawner_zombie", "mobs:spawner_mummy" },
}
local g_tab = {}
for k,_ in pairs(mapgen_elements) do
	g_tab[k] = true
--	minetest.set_gen_notify(k)
end
minetest.set_gen_notify(g_tab)

minetest.register_on_generated(function(minp, maxp, seed)
	local g = minetest.get_mapgen_object("gennotify")
	--print('MOBS GENNOTIFY : '..dump(g))
	for k,v in pairs(mapgen_elements) do
		if g and g[k] and #g[k] > 3 then
			minetest.after(3, function(d)
				if d == nil or #d < 1 then
					return
				end
				for i=1,2 do
					local p = d[math.random(1, #d)]
					if minetest.get_node({x=p.x, y =p.y-1, z=p.z}).name ~= "air" then
						local spwnr = v[math.random(1, #v)]
						if minetest.registered_nodes[spwnr] or minetest.registered_aliases[spwnr] then
							minetest.set_node(p, {name = spwnr})
						end
					end
				end
			end, table.copy(g[k]))
		end
	end
end)
