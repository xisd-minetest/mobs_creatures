--Adapted form Penguin by D00Med
-- TODO hunt fish


-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..S("Penguins are funny little animals who lives in snowy regions. They likes to eat fishs and to slide on ice.")
usagehelp = ''
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.feed_with(S("fish"))
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.tamed_catch_net_or_lasso


local mob_id = modname..":penguin"
mobs:register_mob(mob_id, {
	type = "animal",
	passive = true,
	reach = 1,
	hp_min = 5,
	hp_max = 10,
	armor = 200,
	collisionbox = {-0.2, -0.0, -0.2,  0.2, 0.5, 0.2},
	visual = "mesh",
	mesh = "mobs_penguin.b3d",
	visual_size = {x = 0.25, y = 0.25},
	textures = {
		{"mobs_penguin.png"},
	},
	sounds = {},
	makes_footstep_sound = true,
	walk_velocity = 1,
	run_velocity = 2,
	runaway = true,
	jump = false,
	stepheight = 1.1,
	drops = {
		{name = "mobs:meat_raw", chance = 1, min = 1, max = 1},
	},
	water_damage = 0,
	lava_damage = 4,
	light_damage = 0,
	fear_height = 2,
	animation = {
		speed_normal = 15,
		stand_start = 1,
		stand_end = 20,
		walk_start = 25,
		walk_end = 45,
		fly_start = 75, -- swim animation
		fly_end = 95,
		-- 50-70 is slide/water idle
	},
	fly_in = {"default:water_source", "default:water_flowing"},
	floats = 0,
	follow = {"ethereal:fish_raw", "mobs_fish:clownfish", "mobs_fish:tropical",
				modname..":clownfish", modname..":fish_tropical",
				"mobs:clownfish", "mobs:fish_tropical"},
	view_range = 5,

	on_rightclick = function(self, clicker)
		-- Feed, Protect, or Tame
		if mobs:feed_tame(self, clicker, 4, false, true) then return end
		if mobs:protect(self, clicker) then return end
		if mobs:capture_mob(self, clicker, 0, 40, 30, false, nil) then return end
	end,
})


mobs:spawn({
	name = mob_id,
	nodes = {"default:snowblock"},
	min_light = 10,
	chance = 20000,
	min_height = 0,
	day_toggle = true,
})


mobs:register_egg(mob_id, S("Penguin"), "mobs_penguin_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

mobs:alias_mob("mobs:penguin", mob_id) -- compatibility
