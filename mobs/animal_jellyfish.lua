-- Jellyfishs
-- TODO make them harm players if too close


-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_water
usagehelp = ''
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.wild_catch_net


local mob_id = modname..":jellyfish"
mobs:register_mob(mob_id, {
	type = "animal",
	attack_type = "dogfight",
	damage = 5,
	reach = 1,
	hp_min = 5,
	hp_max = 10,
	armor = 100,
	collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
	visual = "mesh",
	mesh = "mobs_jellyfish.b3d",
	textures = {
		{"mobs_jellyfish.png"}
	},
	makes_footstep_sound = false,
	walk_velocity = 0.1,
	run_velocity = 0.1,
	fly = true,
	fly_in = "default:water_source",
	fall_speed = 0,
	view_range = 10,
	water_damage = 0,
	lava_damage = 5,
	light_damage = 0,
	on_rightclick = function(self, clicker)
		mobs:capture_mob(self, clicker, 3, 40, 0, true, nil)
	end
})
--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
mobs:spawn_specific(mob_id,
	{"default:water_source"},
	{"default:water_flowing","default:water_source"},
	5, 20, 30, 10000, 1, -31000, 0)

mobs:register_egg(mob_id, "Jellyfish", "mobs_jellyfish_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

mobs:alias_mob("mobs:jellyfish", mob_id) -- compatibility
