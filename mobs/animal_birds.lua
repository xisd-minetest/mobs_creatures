-- Small Birds, Larges birds, Gulls
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Other variables
local mob_id

--
-- Documentation for help/doc modpack
--
-- Gulls
-- TODO Make them eat fish
local longdesc = {}
local usagehelp = {}
longdesc.gull = ''
longdesc.gull = longdesc.gull..'\n'..mobs.textstrings.longdesc.peacefull_sky
usagehelp.gull = ''
usagehelp.gull = usagehelp.gull..'\n'..mobs.textstrings.usagehelp.wild_catch_net

-- Other brids
longdesc.bird = ''
longdesc.bird = longdesc.bird..'\n'..mobs.textstrings.longdesc.peacefull_sky
usagehelp.bird = ''
usagehelp.bird = usagehelp.bird..'\n'..mobs.textstrings.usagehelp.wild_catch_net

--
-- Minetest settings
--
local ENABLE_GULLS		 = minetest.settings:get_bool("mobs.animal_birds.enable_gulls") or true
local ENABLE_LARGE_BIRDS = minetest.settings:get_bool("mobs.animal_birds.enable_large_birds") or false
local ENABLE_SMALL_BIRDS = minetest.settings:get_bool("mobs.animal_birds.enable_small_birds")or true

--
-- local variables
--
local l_skins_gull = {
	{"mobs_gull_mesh.png"},
	{"mobs_gull_black.png"},
	{"mobs_gull_gray.png"},
	{"mobs_gull_grayblue.png"}
}
local l_skins_bird = {
	{"mobs_bird_blueish.png"},
	{"mobs_bird_brown.png"},
	{"mobs_bird_gray.png"},
	{"mobs_bird_grayblue.png"},
	{"mobs_bird_red.png"},
	{"mobs_bird_redish.png"}
}
local l_anims = {
	speed_normal = 24,	speed_run = 24,
	stand_start = 1,	stand_end = 95,
	walk_start = 1,		walk_end = 95,
	run_start = 1,		run_end = 95
}
local l_model				= "mobs_gull.b3d"
local l_egg_texture_bird	= "mobs_bird_inv.png"
local l_egg_texture_gull	= "mobs_gull_inv.png"
local l_capture_chance_h	= 1
local l_capture_chance_n	= 30
local l_spawn_in			= {"air"}
local l_spawn_near_gull		= {"default:water_source", "default:water_flowing"}
local l_spawn_near_bird		= {"default:leaves", "default:pine_needles", "default:jungleleaves", "default:cactus"}
local l_spawn_chance_gull	= 24000
local l_spawn_chance_bird	= 36000
local l_drop = {
		{name = "mobs:feather", chance = 3, min = 1, max = 2},
	}

-- load settings
if not ENABLE_LARGE_BIRDS then
	l_spawn_chance_bird = l_spawn_chance_bird - 18000
end
if not ENABLE_SMALL_BIRDS then
	l_spawn_chance_bird = l_spawn_chance_bird - 18000
end

-----------------------------------------------------------------------
-- Gulls
-----------------------------------------------------------------------

if ENABLE_GULLS then
	longdesc = longdesc.gull
	usagehelp = usagehelp.gull
	mob_id = modname..":gull"
	mob_name = S("Gull")
	mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 5,
		hp_max = 10,
		armor = 100,
		collisionbox = {-1, -0.3, -1, 1, 0.3, 1},
		visual = "mesh",
		mesh = l_model,
		textures = l_skins_gull,
		rotate = 270,
		walk_velocity = 4,
		run_velocity = 6,
		fall_speed = 0,
		stepheight = 3,
		drops = l_drop,
		fly = true,
		water_damage = 0,
		lava_damage = 10,
		light_damage = 0,
		view_range = 14,
		animation = l_anims,
		on_rightclick = function(self, clicker)
			mobs:capture_mob(self, clicker, l_capture_chance_h, l_capture_chance_n, 0, true, nil)
		end
	})
	--name, nodes, neighbors, min_light, max_light, interval, chance, active_object_count, min_height, max_height
	mobs:spawn({
		name = mob_id,
		nodes = l_spawn_in,
		neighbors = l_spawn_near_gull,
		min_light = 10,
		interval = 90, --30
		chance = l_spawn_chance_gull,
		active_object_count = 1,
		min_height = 0,
		max_height = 31000,
		day_toggle = true,
	})
	mobs:register_egg(mob_id, S("Gull"), l_egg_texture_gull, 0)
	mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)
	mobs:custom_captured_name(mob_id,S("Wild @1",mob_name))
end

-----------------------------------------------------------------------
-- Large Birds
-----------------------------------------------------------------------
if ENABLE_LARGE_BIRDS then
	longdesc = longdesc.bird
	usagehelp = usagehelp.bird
	mob_id = modname..":bird_lg"
	mob_name = S("Bird")
	mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 5,
		hp_max = 10,
		armor = 100,
		collisionbox = {-0.5, -0.3, -0.5, 0.5, 0.3, 0.5},
		visual = "mesh",
		mesh = l_model,
		textures = l_skins_bird,
		visual_size = {x=.5, y=.5},
		rotate = 270,
		walk_velocity = 4,
		run_velocity = 6,
		fall_speed = 0,
		stepheight = 3,
		drops = l_drop,
		fly = true,
		water_damage = 0,
		lava_damage = 10,
		light_damage = 0,
		view_range = 12,
		animation = l_anims,
		on_rightclick = function(self, clicker)
			mobs:capture_mob(self, clicker, l_capture_chance_h, l_capture_chance_n, 0, true, nil)
		end
	})
	--name, nodes, neighbors, min_light, max_light, interval, chance, active_object_count, min_height, max_height
	--mobs:spawn_specific(mob_id, l_spawn_in, l_spawn_near_bird, 5, 20, 30, l_spawn_chance_bird, 1, 0, 31000)
		mobs:spawn({
		name = mob_id,
		nodes = l_spawn_in,
		neighbors = l_spawn_near_bird,
		min_light = 10,
		interval = 90, --30
		chance = l_spawn_chance_bird,
		active_object_count = 1,
		min_height = 0,
		max_height = 31000,
		day_toggle = true,
	})
	mobs:register_egg(mob_id, "Large bird", l_egg_texture_bird, 0)
	mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)
	mobs:custom_captured_name(mob_id,S("Wild @1",mob_name))
end

-----------------------------------------------------------------------
-- Small Birds
-----------------------------------------------------------------------
if ENABLE_SMALL_BIRDS then
	longdesc = longdesc.bird
	usagehelp = usagehelp.bird
	mob_id = modname..":bird_sm"
	mob_name = S("Bird")
	mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 2,
		hp_max = 5,
		armor = 100,
		collisionbox = {-0.25, -0.3, -0.25, 0.25, 0.3, 0.25},
		visual = "mesh",
		mesh = l_model,
		textures = l_skins_bird,
		visual_size = {x=.25, y=.25},
		rotate = 270,
		walk_velocity = 4,
		run_velocity = 6,
		fall_speed = 0,
		stepheight = 3,
		drops = l_drop,
		fly = true,
		water_damage = 0,
		lava_damage = 10,
		light_damage = 0,
		view_range = 10,
		animation = l_anims,
		on_rightclick = function(self, clicker)
			mobs:capture_mob(self, clicker, l_capture_chance_h, l_capture_chance_n, 0, true, nil)
		end
	})
	--name, nodes, neighbors, min_light, max_light, interval, chance, active_object_count, min_height, max_height
	mobs:spawn({
		name = mob_id,
		nodes = l_spawn_in,
		neighbors = l_spawn_near_bird,
		min_light = 10,
		interval = 90, --30
		chance = l_spawn_chance_bird,
		active_object_count = 1,
		min_height = 0,
		max_height = 31000,
		day_toggle = true,
	})
	mobs:register_egg(mob_id, "Small bird", l_egg_texture_bird, 0)
	mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)
	mobs:custom_captured_name(mob_id,S("Wild @1",mob_name))
end

