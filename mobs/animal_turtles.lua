-- Turtles
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Other variables
local mob_id, mob_name, longdesc_seaturtles, usagehelp_seaturtle, longdesc_landturtle, usagehelp_landturtle, longdesc, usagehelp

--
-- Documentation for help/doc modpack
--
-- Land turtles
longdesc_landturtle = ''
longdesc_landturtle = longdesc_landturtle..'\n'..mobs.textstrings.longdesc.peacefull_land
longdesc_landturtle = longdesc_landturtle..'\n'..S(".")
usagehelp_landturtle = ''
usagehelp_landturtle = usagehelp_landturtle..'\n'..mobs.textstrings.usagehelp.wild_catch_net_or_lasso

-- Sea turtles
-- TODO make them eat jellyfish
longdesc_seaturtle = ''
longdesc_seaturtle = longdesc_seaturtle..'\n'..mobs.textstrings.longdesc.peacefull_water
usagehelp_seaturtle = ''
usagehelp_seaturtle = usagehelp_seaturtle..'\n'..mobs.textstrings.usagehelp.wild_catch_net_or_lasso


local l_colors = {
	"#604000:175",	--brown
	"#604000:100",	--brown2
	"#ffffff:150",	--white
	"#404040:150",	--dark_grey
	"#a0a0a0:150",	--grey
	"#808000:150",	--olive
	"#ff0000:150"	--red
}
local l_skins = {
	{"mobs_turtle1.png^mobs_turtle2.png^mobs_turtle3.png^mobs_turtle4.png^mobs_turtle5.png^mobs_turtle6.png^mobs_turtle7.png"},
	{"mobs_turtle1.png^(mobs_turtle2.png^[colorize:"..l_colors[5]..")^(mobs_turtle3.png^[colorize:"..l_colors[4]..")^(mobs_turtle4.png^[colorize:"..l_colors[1]..")^(mobs_turtle5.png^[colorize:"..l_colors[2]..")^(mobs_turtle6.png^[colorize:"..l_colors[6]..")^mobs_turtle7.png"}
}
local l_anims = {
	speed_normal = 24,	speed_run = 24,
	stand_start = 1,	stand_end = 50,
	walk_start = 60,	walk_end = 90,
	run_start = 60,		run_end = 90,
	hide_start = 95,	hide_end = 100
}
local l_model			= "mobs_turtle.x"
local l_spawn_chance	= 30000

------------------------------------------------------------------------
-- land turtle
------------------------------------------------------------------------
longdesc = longdesc_landturtle
usagehelp = usagehelp_landturtle
mob_id = modname..":turtle"
mob_name = S("Turtle")

mobs:register_mob(mob_id, {
	type = "animal",
	passive = true,
	hp_min = 15,
	hp_max = 20,
	armor = 200,
	collisionbox = {-0.4, 0.0, -0.4, 0.4, 0.35, 0.4},
	visual = "mesh",
	mesh = l_model,
	textures = l_skins,
	makes_footstep_sound = false,
	view_range = 8,
	rotate = 270,
	walk_velocity = 0.1,
	run_velocity = 0.3,
	jump = false,
	fly = false,
	floats = 1,
	water_damage = 0,
	lava_damage = 5,
	light_damage = 0,
	fall_damage = 1,
	animation = l_anims,
	follow = {"farming:carrot","default:jungle_grass"},
	on_rightclick = function(self, clicker)
		self.state = ""
		self.object:set_velocity({x=0,y=0,z=0})
		self.object:set_animation({x=self.animation.hide_start, y=self.animation.hide_end}, self.animation.speed_normal, 0)
		local t = math.random(4,12)
		minetest.after(t, function() 
			self.state = "stand"
		end)
		mobs:capture_mob(self, clicker, 0, 40, 30, true, nil)
	end
})
--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
mobs:spawn_specific(mob_id,
	{"default:dirt_with_grass","default:jungle_grass","default:sand","default:desert_sand"},
	{"default:dirt_with_grass","default:jungle_grass","default:sand","default:desert_sand","default:papyrus","default:cactus","dryplants:juncus","dryplants:reedmace"},
	5, 20, 150, l_spawn_chance, 1, 1, 31000)
mobs:register_egg(mob_id, mob_name, "mobs_turtle_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)
mobs:custom_captured_name(mob_id,S("Wild @1",mob_name))


mobs:alias_mob("mobs:turtle", mob_id) -- compatibility
------------------------------------------------------------------------
-- sea turtle
------------------------------------------------------------------------
longdesc = longdesc_seaturtle
usagehelp = usagehelp_seaturtle
mob_id = modname..":seaturtle"
mob_name = S("Sea Turtle")
mobs:register_mob(mob_id, {
	type = "animal",
	passive = true,
	hp_min = 20,
	hp_max = 30,
	armor = 250,
	collisionbox = {-0.8, 0.0, -0.8, 0.8, 0.7, 0.8},
	visual = "mesh",
	visual_size = {x=2,y=2},
	mesh = l_model,
	textures = l_skins,
	makes_footstep_sound = false,
	view_range = 10,
	rotate = 270,
	walk_velocity = 1,
	run_velocity = 1.5,
	stepheight = 1,
	jump = false,
	fly = true,
	fly_in = "default:water_source",
	fall_speed = 0,
	floats = 1,
	water_damage = 0,
	lava_damage = 5,
	light_damage = 0,
	fall_damage = 0,
	animation = l_anims,
	on_rightclick = function(self, clicker)
		mobs:capture_mob(self, clicker, 0, 30, 30, true, nil)
	end
})
--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
mobs:spawn_specific(mob_id,
	{"default:water_flowing","default:water_source"},
	{"default:water_flowing","default:water_source","group:seaplants","seawrecks:woodship","seawrecks:uboot"},
	5, 20, 150, l_spawn_chance, 1, -31000, 0)
mobs:register_egg(mob_id, mob_name, "mobs_seaturtle_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)
mobs:custom_captured_name(mob_id,S("Wild @1",mob_name))

mobs:alias_mob("mobs:seaturtle", mob_id) -- compatibility
