--Adapted from Sheep by PilzAdam, texture converted to minetest by AMMOnym from Summerfield pack
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Minetest settings
--
local ENABLE_RIDING = minetest.settings:get_bool("mobs.animal_sheep.enable_riding") or true
local ENABLE_DYE = minetest.settings:get_bool("mobs.animal_sheep.enable_dye") or true

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_land
longdesc = longdesc..'\n'..S(".")
usagehelp = ''
usagehelp = usagehelp..'\n'..S("Right-click with shears to get wool.")
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.feed_with(S("wheat"))
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.tamed_catch_net_or_lasso
if ENABLE_DYE then usagehelp = usagehelp..'\n'..S("When tamed, can be dyed using right-click.") end
if ENABLE_RIDING then usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.tamed_ride end


local mob_id = modname..":sheep"
local sheep = {}


sheep.stepheight = 0.6

--
-- Colors
--
local all_colours = {
	{"black",      S("Black"),      "#000000b0"}, -- 1
	{"blue",       S("Blue"),       "#015dbb70"}, -- 2 
	{"brown",      S("Brown"),      "#663300a0"}, -- 3
	{"cyan",       S("Cyan"),       "#01ffd870"}, -- 4
	{"dark_green", S("Dark Green"), "#005b0770"}, -- 5
	{"dark_grey",  S("Dark Grey"),  "#303030b0"}, -- 6
	{"green",      S("Green"),      "#61ff0170"}, -- 7
	{"grey",       S("Grey"),       "#5b5b5bb0"}, -- 8
	{"magenta",    S("Magenta"),    "#ff05bb70"}, -- 9
	{"orange",     S("Orange"),     "#ff840170"}, -- 10
	{"pink",       S("Pink"),       "#ff65b570"}, -- 11
	{"red",        S("Red"),        "#ff0000a0"}, -- 12
	{"violet",     S("Violet"),     "#2000c970"}, -- 13
	{"white",      S("White"),      "#abababc0"}, -- 14
	{"yellow",     S("Yellow"),     "#e3ff0070"}, -- 15
}
local wild_colours = { 1, 3, 6, 8, 14 }

sheep.set_color = function(self,colordata)
	self.base_texture = {"mobs_sheep_base.png^(mobs_sheep_wool.png^[colorize:" .. colordata[3] .. ")"}
	self._wool_color = colordata[1] 
	if not self.gotten then 
		self.object:set_properties({textures = self.base_texture})
	end
end

--
-- Riding
--
sheep.ride_on_rightclik = function(self, clicker, itemname)
	if not ENABLE_RIDING then return end
	
	local name = clicker:get_player_name()

	-- Return if not tamed
	if not ( self.tamed and self.owner == clicker:get_player_name() ) then
		minetest.chat_send_player(name, S("Not tamed!"))
		return
	end

	if not (item and itemname) then
		item = clicker:get_wielded_item()
		itemname = item:get_name()
	end
	local inv = clicker:get_inventory()

	-- Are we ridding? --

	-- detatch player already riding horse
	if self.driver and clicker == self.driver then

		mobs.detach(clicker, {x = 1, y = 0, z = 1})
		self.object:set_properties({stepheight = sheep.stepheight})

		-- add saddle back to inventory
		if inv:room_for_item("main", "mobs:saddle") then
			inv:add_item("main", "mobs:saddle")
		else
			minetest.add_item(clicker.getpos(), "mobs:saddle")
		end
		return true
	-- attach player to horse
	elseif not self.driver and itemname == "mobs:saddle" then

		self.object:set_properties({stepheight = 1.1})
		mobs.attach(self, clicker)

		-- take saddle from inventory
		inv:remove_item("main", "mobs:saddle")
		return true
	end
end
sheep.ride_do_custom = function(self, dtime)
	if not ENABLE_RIDING then return end
	-- set needed values if not already present
	if not self.v2 then
		self.v2 = 0
		self.max_speed_forward = 6
		self.max_speed_reverse = 2
		self.accel = 6
		self.terrain_type = 3
		self.driver_attach_at = {x = 0, y = 0, z = -2} -- y == 20
		self.driver_eye_offset = {x = 0, y = 0.5, z = 0}
		self.driver_scale = {x = 1, y = 1}
	end

	-- if driver present allow control of sheep
	if self.driver then

		mobs.drive(self, "walk", "stand", false, dtime)

		return false -- skip rest of mob functions
	end

	return true
end

--
-- Coloring
--
sheep.dye_on_rightclik= function(self, clicker)
			
	local name = clicker:get_player_name()
	if not (item and itemname) then
		item = clicker:get_wielded_item()
		itemname = item:get_name()
	end

	if not itemname:find("dye:") then return end

	local can_dye = ( self.gotten == false 
		and self.child == false
		and self.tamed == true
		and name == self.owner )

	-- Return if not allow
	if not can_dye then 
		minetest.chat_send_player(name, S("Impossible!"))
		return
	end

	local colr = string.split(itemname, ":")[2]

	for _,c in ipairs(all_colours) do

		if c[1] == colr then 
			
			-- set color
			sheep.set_color(self,c)

			-- take item
			if not mobs.is_creative(clicker:get_player_name()) then
				item:take_item()
				clicker:set_wielded_item(item)
			end

			break
		end
	end

	return true
end

mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 8,
		hp_max = 10,
		armor = 200,
		collisionbox = {-0.5, -1, -0.5, 0.5, 0.3, 0.5},
		visual = "mesh",
		mesh = "mobs_sheep.b3d",
		textures = {
			{"mobs_sheep_base.png^(mobs_sheep_wool.png^[colorize:" .. "#abababc0" .. ")"},
		},
		gotten_texture = {"mobs_sheep_shaved.png"},
		gotten_mesh = "mobs_sheep_shaved.b3d",
		makes_footstep_sound = true,
		sounds = {
			random = "mobs_sheep",
		},
		walk_velocity = 1,
		run_velocity = 2,
		runaway = true,
		stepheight = sheep.stepheight,
--		jump = false,
		jump = true,
		drops = {
			{name = "mobs:meat_raw", chance = 1, min = 1, max = 2},
			--{name = "wool:"..col[1], chance = 1, min = 1, max = 1},
		},
		water_damage = 1,
		lava_damage = 5,
		light_damage = 0,
		animation = {
			speed_normal = 15,
			speed_run = 15,
			stand_start = 0,
			stand_end = 80,
			walk_start = 81,
			walk_end = 100,
		},
		follow = {"farming:wheat", "default:grass_5", "default:grass_3"},
		view_range = 8,
		replace_rate = 10,
		replace_what = {"default:grass_3", "default:grass_4", "default:grass_5", "farming:wheat_8"},
		replace_with = "air",
		replace_offset = -1,
		fear_height = 3,
--[[
		on_replace = function(self, pos, oldnode, newnode)
			print ("---- replaced") ; return false -- false to keep node, true to replace
		end,
]]
		-- Select random color on spawn
		on_spawn = function(self)	
			local k = math.random(1,#wild_colours)
			local l = wild_colours[k]
			local c = all_colours[l]
			sheep.set_color(self,c)
			return true -- run only once, false/nil runs every activation
		end,

		do_custom = function(self, dtime)
			if sheep.ride_do_custom(self, dtime) then return true end
		end,

		on_die = function(self, pos)

			-- drop saddle when horse is killed while riding
			-- also detach from horse properly
			if self.driver then
				minetest.add_item(pos, "mobs:saddle")
				mobs.detach(self.driver, {x = 1, y = 0, z = 1})
			end

		end,

		on_rightclick = function(self, clicker)


			-- Are we feeding? --
			if mobs:feed_tame(self, clicker, 8, true, true) then	
				--if full grow fuzz (sometimes)
				if self.gotten == true and math.random(1,3) == 1 then
					self.object:set_properties({
						textures = self.base_texture,
						mesh = "mobs_sheep.b3d",
					})
					self.gotten = false
				end
				return
			end

			local item = clicker:get_wielded_item()
			local itemname = item:get_name()

			-- Are we giving a haircut ? --
			--if itemname == "mobs:shears" or itemname == "vines:shears"  then
			-- Any kind of shears will do
			if itemname:find(":shears")  then

				if self.gotten ~= false
				or self.child ~= false
				or not minetest.get_modpath("wool") then
					return
				end

				self.gotten = true -- shaved

				local obj = minetest.add_item(
					self.object:get_pos(), 
					ItemStack( "wool:" .. self._wool_color .. " " .. math.random(1, 3) )
				)

				if obj then

					obj:setvelocity({
						x = math.random(-1, 1),
						y = 5,
						z = math.random(-1, 1)
					})
				end

				item:add_wear(650) -- 100 uses

				clicker:set_wielded_item(item)

				self.object:set_properties({
					textures = {"mobs_sheep_shaved.png"},
					mesh = "mobs_sheep_shaved.b3d",
				})

				return
			end


			-- Coloring --
			if sheep.dye_on_rightclik(self, clicker, item, itemname) then return end

			-- Riding
			if sheep.ride_on_rightclik(self, clicker, item, itemname) then return end
				
			-- protect mod with mobs:protector item
			if mobs:protect(self, clicker) then return end

			-- Are we capturing? --
			if mobs:capture_mob(self, clicker, 0, 5, 40, false, nil) then return end

		end
	})

mobs:register_egg(mob_id, S("Sheep"), "mobs_sheep_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)


local spawn_on = "default:dirt_with_grass"

if minetest.get_modpath("ethereal") then
	spawn_on = "ethereal:green_dirt"
end

mobs:spawn({
	name = mob_id,
	nodes = {spawn_on},
	min_light = 10,
	interval = 150, --30
	chance = 15000,
	min_height = 0,
	max_height = 31000,
	day_toggle = true,
})


mobs:alias_mob("mobs:sheep", mob_id) -- compatibility
