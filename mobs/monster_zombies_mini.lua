-- Mini Zombies
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

local mob_id = modname..":zombie_mini"

local zombie_do_custom = function(self)
	if self.lifetimer <= self.shoot_interval then
		if math.random(100) <= 50 then
			minetest.add_entity(self.object:getpos(), mob_id)
		end
		self.shoot_interval = self.shoot_interval - 45
	end
end

mobs:register_mob(mob_id, {
	type = "monster",
	visual = "mesh",
	mesh = "mobs_creatures_mob.x",
	textures = {
		{"mobs_zombie.png"},
	},
	visual_size = {x = 0.5, y = 0.5},
	collisionbox = {-0.125, -0.5, -0.15, 0.125, 0.375, 0.15},
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie.1",
		war_cry = "mobs_zombie.3",
		attack = "mobs_zombie.2",
		damage = "mobs_zombie_hit",
		death = "mobs_zombie_death"
	},
	hp_min = 20,
	hp_max = 45,
	armor = 150,
	knock_back = 1,
	lava_damage = 10,
	damage = 6,
	reach = 1,
	attack_type = "dogfight",
	group_attack = true,
	view_range = 10,
	walk_chance = 75,
	walk_velocity = 0.8,
	run_velocity = 0.8,
	jump = false,
	drops = {
		{name = "mobs:rotten_flesh", chance = 1, min = 1, max = 1,}
	}
})

return zombie_do_custom
