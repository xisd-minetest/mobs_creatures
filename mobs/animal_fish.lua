-- Fishes
-- 
-- TODO make them look for coral
-- TODO add stick_fish ?

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local mob_id
--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp

local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_water
usagehelp = ''
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.wild_catch_net

-- setting to use upright sprites instead of meshes
local SPRITE_VERSION = minetest.settings:get_bool("mobs.animal_fish.sprite_version") or false

-- local variables
	local l_spawn_in		= {"default:water_source", "default:water_flowing", "default:river_water_source", "default:river_water_flowing"}
	local l_spawn_near		= {"default:sand","default:dirt","group:seaplants","group:seacoral"}
	local l_spawn_chance	= 10000
	local l_cc_hand			= 25
	local l_cc_net			= 80
	local l_water_level		= minetest.settings:get("water_level") - 1
	local l_anims = {
		speed_normal = 24,		speed_run = 24,
		stand_start = 1,		stand_end = 80,
		walk_start = 81,		walk_end = 155,
		run_start = 81,			run_end = 155
	}
	local l_visual = "mesh"
	local l_visual_size = {x=.75, y=.75}
	local l_clown_mesh = "mobs_clownfish.b3d"
	local l_trop_mesh = "mobs_fish.b3d"	
	local l_clown_textures = {
		{"mobs_clownfish.png"},
		{"mobs_clownfish2.png"}
	}
	local l_trop_textures = {
		{"mobs_fish.png"},
		{"mobs_fish2.png"},
		{"mobs_fish3.png"}
	}
	local l_clown_textures_inv = "mobs_clownfish_inv.png"
	local l_trop_textures_inv = "mobs_fish_tropical_inv.png"

	if SPRITE_VERSION then
		l_visual = "upright_sprite"
		l_visual_size = {x=.5, y=.5}
		l_clown_mesh = nil
		l_trop_mesh = nil		
		l_clown_textures = {{l_clown_textures_inv"mobs_clownfish_inv.png"}}
		l_trop_textures = {{l_clown_textures_inv"mobs_fish_tropical_inv.png"}}
	end

-- Clownfish
	mob_id = modname..":clownfish"
	mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 1,
		hp_max = 4,
		armor = 100,
		collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
		rotate = 270,
		visual = l_visual,
		mesh = l_clown_mesh,
		textures = l_clown_textures,
		visual_size = l_visual_size,
		makes_footstep_sound = false,
		stepheight = 0.1,
		fly = true,
		fly_in = "default:water_source",
		fall_speed = 0,
		view_range = 8,
		water_damage = 0,
		lava_damage = 5,
		light_damage = 0,
		animation = l_anims,
		on_rightclick = function(self, clicker)
			mobs:capture_mob(self, clicker, l_cc_hand, l_cc_net, 0, true, nil)
		end
	})
	--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
	mobs:spawn_specific(mob_id, l_spawn_in, l_spawn_near, 5, 20, 30, l_spawn_chance, 1, -31000, l_water_level)
	mobs:register_egg(mob_id, "Clownfish", l_clown_textures_inv, 0)
	mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

	mobs:alias_mob("mobs:clownfish", mob_id) -- compatibility


-- Tropical fish
	mob_id = modname..":fish_tropical"
	mobs:register_mob(mob_id, {
		type = "animal",
		passive = true,
		hp_min = 1,
		hp_max = 4,
		armor = 100,
		collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
		rotate = 270,
		visual = l_visual,
		mesh = l_trop_mesh,
		textures = l_trop_textures,
		visual_size = l_visual_size,
		makes_footstep_sound = false,
		stepheight = 0.1,
		fly = true,
		fly_in = "default:water_source",
		fall_speed = 0,
		view_range = 8,
		water_damage = 0,
		lava_damage = 5,
		light_damage = 0,
		animation = l_anims,
		on_rightclick = function(self, clicker)
			mobs:capture_mob(self, clicker, l_cc_hand, 100, 0, true, nil)
		end
	})
	--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
	mobs:spawn_specific(mob_id, l_spawn_in, l_spawn_near, 5, 20, 30, l_spawn_chance, 1, -31000, l_water_level)
	mobs:register_egg(mob_id, "Tropical fish", l_trop_textures_inv, 0)
	mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

	mobs:alias_mob("mobs:fish_tropical", mob_id) -- compatibility
