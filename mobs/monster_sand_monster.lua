-- Sand Monster by PilzAdam
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.hostile_custom(S("in deserts and pyramids"))
longdesc = longdesc..'\n'.. mobs.textstrings.longdesc.a_long_time_ago_3
longdesc = longdesc..' '.. mobs.textstrings.longdesc.history_mummy_1
longdesc = longdesc..' '.. mobs.textstrings.longdesc.history_mummy_2

--usagehelp = ''
--usagehelp = usagehelp..'\n'..S(".")

local mob_id = modname..":sand_monster"
mobs:register_mob(mob_id, {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	pathfinding = false,
	--specific_attack = {"player", "mobs_npc:npc"},
	reach = 2,
	damage = 1,
	hp_min = 4,
	hp_max = 20,
	armor = 100,
	collisionbox = {-0.4, -1, -0.4, 0.4, 0.8, 0.4},
	visual = "mesh",
	mesh = "mobs_sand_monster.b3d",
	textures = {
		{"mobs_mummy.png"},
--		{"mobs_sand_monster.png"},
	},
	makes_footstep_sound = true,
	sounds = {
		random = "creatures_mummy", --"mobs_sandmonster",
		war_cry = "mobs_zombie.3",
		attack = "mobs_zombie.2",
		damage = "creatures_mummy_hit",
		death = "creatures_mummy_death",	
	},
	walk_velocity = 0.5,
	run_velocity = 1,
	view_range = 8, --15
	jump = false,
--	jump_height=0,
--	step_height=0,
	runaway = true,
	floats = 0,
	drops = {
		{name = "mobs:rotten_flesh", chance = 7, min = 1, max = 2,},
		{name = "default:papyrus", chance = 6, min = 1, max = 2,},
		{name = "default:paper", chance = 16, min = 1, max = 3,},
		{name = "default:desert_sand", chance = 10, min = 3, max = 5},
	},
	water_damage = 3,
	lava_damage = 4,
	light_damage = 0,
	fear_height = 4,
	animation = {
		speed_normal = 15,
		speed_run = 20,
		stand_start = 0,
		stand_end = 39,
		walk_start = 74, 	--41,
		walk_end = 105,		--72,
		run_start = 74,
		run_end = 105,
		punch_start = 74,
		punch_end = 105,
	},
--[[
	custom_attack = function(self, p)
		local pos = self.object:get_pos()
		minetest.add_item(pos, "default:sand")
	end,
]]
})

mobs:spawn({
	name = mob_id,
	nodes = {"default:sand", "default:desert_sand", "default:silver_sand", "default:desert_stone"},
	neighbors = {"air"},
	chance = 20000,
	active_object_count = 2,
	max_light = 5,
	max_height = 100,
	day_toggle = false,
})


mobs:register_egg(mob_id, S("Sand Monster"), "mobs_mummy_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)


mobs:alias_mob("mobs:sand_monster", mob_id) -- compatibility
mobs:alias_mob("mobs:mummy", mob_id) -- compatibility
