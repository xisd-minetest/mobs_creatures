-- Adapted from Oerkki by PilzAdam
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
--- --[ Documentation for help/doc modpack ]--
--  ------------------------------------------
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.hostile_dark
longdesc = longdesc..'\n'..S("Hurted by bright light")..', '
longdesc = longdesc..' '..S("they thrive in darkness and will blow torches out on their way.")
longdesc = longdesc..'\n'.. mobs.textstrings.longdesc.a_long_time_ago_2
longdesc = longdesc..' '.. mobs.textstrings.longdesc.history_oerkki_1
--usagehelp = ''
--usagehelp = usagehelp..'\n'..S(".")


--
--- --[ Variables ]--
--  ------------------------------------------

local mob_id = modname..":oerkki"

-- Will not spawn above this height
local max_spawn_height = -200
local dm = 1
if mobs.mobs_bundle_settings.damages.nopositional then dm = 2 end

--
--- --[ Definition ]--
--  ------------------------------------------

mobs:register_mob(mob_id, {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	pathfinding = true,
	reach = 1,
	damage = 2*dm,
	hp_min = 4*dm,
	hp_max = 16*dm,
	armor = 100,
	collisionbox = {-0.4, -1, -0.4, 0.4, 0.9, 0.4},
	visual = "mesh",
	mesh = "mobs_oerkki.b3d",
	textures = {
		{"mobs_oerkki.png"},
		{"mobs_oerkki2.png"},
	},
	makes_footstep_sound = false,
	sounds = {
		random = "mobs_oerkki",
	},
	walk_velocity = 1,
	run_velocity = 3,
	view_range = 10,

	step_height = 0.6,
	jump = true,
	jump_height = 1.1,

	drops = {
		{name = "default:obsidian", chance = 18, min = 1, max = 2},
		{name = "default:obsidian_shard", chance = 3, min = 1, max = 2},
	},
	water_damage = 2,
	lava_damage = 4,
	light_damage = 1,
	fear_height = 4,
	animation = {
		stand_start = 0,
		stand_end = 23,
		walk_start = 24,
		walk_end = 36,
		run_start = 37,
		run_end = 49,
		punch_start = 37,
		punch_end = 49,
		speed_normal = 15,
		speed_run = 15,
	},
	replace_rate = 5,
	replace_what = {"default:torch"},
	replace_with = "air",
	replace_offset = -1,
	immune_to = {
		{"default:sword_wood", 0}, -- no damage
		{"default:gold_lump", -10}, -- heals by 10 points
	},
	on_die = function(self, pos) 
		if math.random(1,5) < 2 then mobs:drop_something_from_treasurer(self, pos, max_spawn_height) end
	end
})


--
--- --[ Spawn ]--
--- ------------------------------------------

mobs:spawn({
	name = mob_id,
	nodes = {"default:stone"},
	max_light = 4,
	chance = 17000,
	interval = 50,
	max_height = max_spawn_height,
	day_toggle = false,
	on_spawn = mobs.custom_on_spawn,
})

--- --[ Nether Spawn ]--

if minetest.get_modpath('nether') then 

	local NETHER_DEPTH = tonumber(minetest.settings:get("nether_depth")) or -5000

	mobs:spawn({
		name = mob_id,
		nodes = {"nether:rack"},
		max_light = 8,
		chance = 1700,
		interval = 50,
		max_height = NETHER_DEPTH,
	})
end


--
--- --[ Egg ]--
--- ------------------------------------------

mobs:register_egg(mob_id, S("Oerkki"), "mobs_oerkki_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

--
--- --[ Alias ]--
--- ------------------------------------------

mobs:alias_mob("mobs:oerkki", mob_id) -- compatiblity
