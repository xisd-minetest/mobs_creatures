-- Adapted from Chicken by JK Murray


-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local flesh, longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_land
longdesc = longdesc..'\n'..S("Chicken are birds, they lay eggs, they like eating seeds and they will like you if you give them some.")
usagehelp = ''
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.feed_with(S("seeds"))
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.tamed_catch_net_or_lasso

--
-- Settings
--
local CHICKEN_MEAT = minetest.settings:get_bool("mobs.animal_chicken.enable_chicken_meat") or false


local mob_id = modname..":chicken"

if not CHICKEN_MEAT then flesh = "mobs:chicken_raw" else  flesh = "mobs:meat_raw" end

local chicken_drop = {
		{name = "mobs:feather", chance = 5, min = 1, max = 2},
		{name = flesh, chance = 6, min = 1, max = 2},
	}

mobs:register_mob(mob_id, {
	type = "animal",
	passive = true,
	hp_min = 5,
	hp_max = 10,
	armor = 200,
	collisionbox = {-0.3, -0.75, -0.3, 0.3, 0.1, 0.3},
	visual = "mesh",
	mesh = "mobs_chicken.x",
	-- seems a lot of textures but this fixes the problem with the model
	textures = {
		{"mobs_chicken.png", "mobs_chicken.png", "mobs_chicken.png", "mobs_chicken.png",
		"mobs_chicken.png", "mobs_chicken.png", "mobs_chicken.png", "mobs_chicken.png", "mobs_chicken.png"},
		{"mobs_chicken_black.png", "mobs_chicken_black.png", "mobs_chicken_black.png", "mobs_chicken_black.png",
		"mobs_chicken_black.png", "mobs_chicken_black.png", "mobs_chicken_black.png", "mobs_chicken_black.png", "mobs_chicken_black.png"},
	},
	child_texture = {
		{"mobs_chick.png", "mobs_chick.png", "mobs_chick.png", "mobs_chick.png",
		"mobs_chick.png", "mobs_chick.png", "mobs_chick.png", "mobs_chick.png", "mobs_chick.png"},
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_chicken",
	},
	walk_velocity = 1,
	run_velocity = 3,
	runaway = true,
	drops = chicken_drop,
	water_damage = 1,
	lava_damage = 5,
	light_damage = 0,
	fall_damage = 0,
	fall_speed = -8,
	fear_height = 5,
	animation = {
		speed_normal = 15,
		stand_start = 0,
		stand_end = 1, -- 20
		walk_start = 20,
		walk_end = 40,
	},
	follow = {"farming:seed_wheat", "farming:seed_cotton",
				"crops:pumpkin_seed", "crops:tomato_seed", "crops:green_bean_seed",
				"crops:melon_seed", "crops:potato_eyes", "crops:corn"},
	view_range = 5,
	replace_rate = 10,
	replace_what = {"group:flora", "group:horsetail", "group:seed", "group:plant"},
	replace_with = "air",
	replace_offset = 0,
	
	--~ on_replace = function(self, pos, oldnode, newnode)
		--~ --return false to keep node, true to replace
		--~ return false 
	--~ end,
	
	on_rightclick = function(self, clicker)
	
		-- Grow new feathers
		if mobs:feed_tame(self, clicker, 8, true, true) then	
			-- New Feather whan eating (sometimes)
			if self.gotten == true and math.random(1,3) == 1 then
				self.gotten = false
			end
			return
		end
		
		if mobs:protect(self, clicker) then return end
		if mobs:capture_mob(self, clicker, 5, 30, 30, false, nil) then return end

		-- Take feathers
		if not self.gotten then
			local item = "mobs:feather"
			local num = math.random(1,3)
			local pos = self.object:get_pos()
			for n=1,num do
				local p = {x=pos.x + math.random(-1, 1), y=pos.y + 1, z=pos.z + (math.random(-1, 1))}
				minetest.add_item(p, item)
			end
			minetest.sound_play("mobs_chicken", {
				pos = pos,
				gain = 0.7,
				max_hear_distance = 3,
			})
			self.gotten = true
		end
	end,

	do_custom = function(self, dtime)
	
		self.egg_timer = (self.egg_timer or 0) + dtime
		if self.egg_timer < 10 then
			return
		end
		self.egg_timer = 0

		if self.child
		or math.random(1, 100) > 1 then
			return
		end

		local pos = self.object:get_pos()

		minetest.add_item(pos, "mobs:egg")

		minetest.sound_play("default_place_node_hard", {
			pos = pos,
			gain = 1.0,
			max_hear_distance = 5,
		})
	end,
})


local spawn_on = {"default:dirt_with_grass", "default:dirt_with_rainforest_litter",
	"default:dirt_with_coniferous_litter", "ethereal:bamboo_dirt"}


mobs:spawn({
	name = mob_id,
	nodes = spawn_on,
	min_light = 10,
	interval = 150, --30
	chance = 25000,
	active_object_count = 2,
	min_height = 0,
	day_toggle = true,
})


mobs:register_egg(mob_id, S("Chicken"), "mobs_chicken_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)


mobs:alias_mob("mobs:chicken", mob_id) -- compatibility

--[[
-- egg entity

mobs:register_arrow(modname..":egg_entity", {
	visual = "sprite",
	visual_size = {x=.5, y=.5},
	textures = {"mobs_chicken_egg.png"},
	velocity = 6,

	hit_player = function(self, player)
		player:punch(minetest.get_player_by_name(self.playername) or self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 1},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(minetest.get_player_by_name(self.playername) or self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 1},
		}, nil)
	end,

	hit_node = function(self, pos, node)

		if math.random(1, 10) > 1 then
			return
		end

		pos.y = pos.y + 1

		local nod = minetest.get_node_or_nil(pos)

		if not nod
		or not minetest.registered_nodes[nod.name]
		or minetest.registered_nodes[nod.name].walkable == true then
			return
		end

		local mob = minetest.add_entity(pos, mob_id)
		local ent2 = mob:get_luaentity()

		mob:set_properties({
			textures = ent2.child_texture[1],
			visual_size = {
				x = ent2.base_size.x / 2,
				y = ent2.base_size.y / 2
			},
			collisionbox = {
				ent2.base_colbox[1] / 2,
				ent2.base_colbox[2] / 2,
				ent2.base_colbox[3] / 2,
				ent2.base_colbox[4] / 2,
				ent2.base_colbox[5] / 2,
				ent2.base_colbox[6] / 2
			},
		})

		ent2.child = true
		ent2.tamed = true
		ent2.owner = self.playername
	end
})


--]]
