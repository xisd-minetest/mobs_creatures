-- Slimes
-- TODO change drops, attact player who holds something

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
local longdesc, usagehelp
longdesc = ''
--longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_land
usagehelp = ''
usagehelp = usagehelp..'\n'


-- sounds
local green_sounds = {
	damage = "slimes_damage",
	death = "slimes_death",
	jump = "slimes_jump",
	attack = "slimes_attack"
}

local l_spawn_elevation_min = tonumber(minetest.settings:get("water_level"))
if l_spawn_elevation_min then
	l_spawn_elevation_min = l_spawn_elevation_min + 2
else
	l_spawn_elevation_min = 2
end 

-- textures
local green_textures = {"mobs_green_slime_sides.png", "mobs_green_slime_sides.png", "mobs_green_slime_sides.png",
	"mobs_green_slime_sides.png", "mobs_green_slime_front.png", "mobs_green_slime_sides.png"}

-- local green_attack = { modname..":sheep", modname..":bunny", modname..":chicken", modname..":gull", modname..":bird_sm", modname..":bird_lg", modname..":kitten", modname..":penguin", modname..":turtle", modname.."_npcs:npc"}
local green_attack = { modname..":bunny", modname..":gull", modname..":bird_sm", modname..":bird_lg", modname..":kitten", modname..":penguin", modname..":turtle", modname.."_npcs:npc"}

local green_def = {
	--type = "animal",
	type = "monster",
	passive = true,
	visual = "cube",
	textures = { green_textures },
	sounds = green_sounds,
	armor = 100,
	blood_amount = 3,
	blood_texture = "mobs_green_slime_blood.png",
	fall_damage = 0,
	attack_type = "dogfight",
	-- attacks_monsters = true		-- usually for npc's to attack monsters in area
    group_attack = true, 	-- to defend same kind of mobs from attack in area
    attack_animals = true,  -- for monster to attack animals as well as player and npc's
	specific_attack = green_attack,
	-- docile_by_day = true,
	view_range = 10,
	walk_chance = 0,
	walk_velocity = 2,
	jump_chance = 60,
}

-------------------------------------------------
-- Small
-------------------------------------------------

local green_small_id = modname..":green_small"

local green_small_def = green_def
green_small_def.visual_size = {x = 0.5, y = 0.5}
green_small_def.collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25}
green_small_def.hp_min = 2
green_small_def.hp_max = 4
green_small_def.knock_back = 3
green_small_def.blood_amount = 3
green_small_def.lava_damage = 3
green_small_def.damage = 1
green_small_def.reach = 1
green_small_def.stepheight = 0.6
green_small_def.drops = {
		{name = "mobs:green_slimeball", chance = 3, min = 1, max = 2},
	}
	
green_small_def.on_die = function(self, pos) 
	if math.random(1,5) < 2 then mobs:drop_something_from_treasurer(self, pos, l_spawn_elevation_min) end
end

mobs:register_mob(green_small_id,green_small_def)

mobs:spawn({
	name = green_small_id,
	nodes = {"default:dirt_with_grass", "default:junglegrass", 
		--"default:mossycobble",
		"ethereal:green_dirt_top"},
	neighbors = {"air"},
	min_light = 6,
	interval = 1500, --30
	chance = 4000,
	active_object_count= 3,
	min_height = l_spawn_elevation_min,
	max_height = 31000,
	day_toggle = true,
})

mobs:register_egg(green_small_id, "Small Green Slime", "mobs_green_slime_inv.png", 0)
mobs:doc_identifier_compat(green_small_id, longdesc, usagehelp)

-------------------------------------------------
-- Medium
-------------------------------------------------
local green_medium_id = modname..":green_medium"
green_medium_def = green_def
green_medium_def.visual_size = {x = 1, y = 1}
green_medium_def.collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}
green_medium_def.hp_min = 4
green_medium_def.hp_max = 8
green_medium_def.knock_back = 2
green_medium_def.blood_amount = 4
green_medium_def.lava_damage = 7
green_medium_def.damage = 2
green_medium_def.reach = 2
green_medium_def.stepheight = 1.1
green_medium_def.on_die = function(self, pos)
	-- inherit attack state and target
	local staticdata = {}
	if self.state == 'attack' then 
		staticdata = {
			attack = self.attack,
			state = self.state,
			}
	end

	local num = math.random(2, 4)
	for i=1,num do
		local p = {x=pos.x + math.random(-2, 2), y=pos.y + 1, z=pos.z + (math.random(-2, 2))}
		local obj = minetest.add_entity(p, modname..":green_small")
			
	end
end

mobs:register_mob(green_medium_id, green_medium_def)


mobs:spawn({
	name = green_medium_id,
	nodes = {"default:dirt_with_grass", "default:junglegrass", "ethereal:green_dirt_top"},
	neighbors = {"air"},
	min_light = 6,
	interval = 180,
	chance = 8000,
	min_height = l_spawn_elevation_min,
	max_height = 31000,
	day_toggle = true,
})

mobs:register_egg(green_medium_id, "Medium Green Slime", "mobs_green_slime_inv.png", 0)
mobs:doc_identifier_compat(green_medium_id, longdesc, usagehelp)
