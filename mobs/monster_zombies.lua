-- Zombies
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'.. mobs.textstrings.longdesc.hostile_dark
longdesc = longdesc..'\n'.. mobs.textstrings.longdesc.a_long_time_ago_1
longdesc = longdesc..' '.. mobs.textstrings.longdesc.history_zombie_1

usagehelp = ''
usagehelp = usagehelp..'\n'..S("")


local mob_id = modname..":zombie"
local dm = 1
if mobs.mobs_bundle_settings.damages.nopositional then dm = 2 end

local ENABLE_MINI_ZOMBIE = minetest.settings:get_bool("mobs.monster_zombie.enable_mini_zombies") or false
local zombie_do_custom
if ENABLE_MINI_ZOMBIE then 
	zombie_do_custom = dofile(modpath .. "/monster_zombies_mini.lua") -- D00Med
end

-- Will not spawn above this height
local max_spawn_height = -15

-- zombie
mobs:register_mob(mob_id, {
	type = "monster",
	visual = "mesh",
	mesh = "mobs_creatures_mob.x",
	textures = {
		{"mobs_zombie.png"},
	},
	collisionbox = {-0.25, -1, -0.3, 0.25, 0.75, 0.3},
	animation = {
		speed_normal = 10,		speed_run = 15,
		stand_start = 0,		stand_end = 79,
		walk_start = 168,		walk_end = 188,
		run_start = 168,		run_end = 188
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_zombie.1",
		war_cry = "mobs_zombie.3",
		attack = "mobs_zombie.2",
		damage = "mobs_zombie_hit",
		death = "mobs_zombie_death",
	},
	hp_min = 5*dm,
	hp_max = 15*dm,
	armor = 200,
	knock_back = 1,
	lava_damage = 10,
	damage = 2*dm,
	reach = 1,
	attack_type = "dogfight",
	group_attack = true,
	view_range = 10,
	walk_chance = 75,
	walk_velocity = 0.5,
	run_velocity = 0.6,
	step_height = 0.6,
	jump = true,
	jump_height = 1.1,
	
	drops = {
		{name = "mobs:rotten_flesh", chance = 6, min = 1, max = 3,}
	},
	lifetimer = 180,		-- 3 minutes
	shoot_interval = 135,	-- (lifetimer - (lifetimer / 4)), borrowed for do_custom timer
	do_custom = zombie_do_custom,
	--on_breed = function(self, ent) return false end
	on_die = function(self, pos) 
		mobs:drop_something_from_treasurer(self, pos, max_spawn_height)
	end
	
})

mobs:spawn({
	name = mob_id,
	nodes = {"default:stone","nether:rack"},
	neighbors = {"air"},
	--~ min_light = -1,
	max_light = 5,
	interval = 500,
	chance = 2000,
	active_object_count = 2,
	max_height = max_spawn_height,
	day_toggle = false,
	on_spawn = mobs.custom_on_spawn,
})


if minetest.get_modpath('nether') then 

	local NETHER_DEPTH = tonumber(minetest.settings:get("nether_depth")) or -5000

	mobs:spawn({
		name = mob_id,
		nodes = {"nether:rack"},
		max_light = 8,
		chance = 1700,
		active_object_count = 2,
		interval = 50,
		max_height = NETHER_DEPTH,
	})
end

mobs:register_egg(mob_id, "Zombie", "mobs_zombie_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)

mobs:alias_mob("mobs:zombie", mob_id) -- compatibility

mobs:register_special_spawner({
	id =  "zombie",
	description = S("Zombie Spawner"),
	longdesc = S("Hostiles Zombie will spawn around this block"),
	-- abm definition
	neighbors = {"air"},
	--interval = 2.0,
	-- chance =  20,
	--min_light = -1,
	max_light = 4,
	-- min_height = -31000,
	-- max_height =  31000,
	-- day_toggle = false,
	-- on_spawn nil,
	-- Range of spawner
	-- range = 17,
	-- max_mobs  = 6,
	-- base texture for the spawner
	texture_face = "all", --"sides",
	base_texture = "default_stone_block.png",--"default_stone_block.png",
	-- Image drawn on the spawner block
	glyph = "mobs_zombie_spawner.png",
	-- Number of mobs spawned when spawner is destructed
	-- last_spawn = 1
	-- Chances that spawner drop nothing
	-- empty_drop_chance = 2
	-- Mob Spawned by spawner
	mob = mob_id,    
	tiles = {
		"default_stone_block.png", -- Top
		"default_stone_block.png^mobs_zombie_spawner.png", -- Bottom
		"default_stone_block.png",
		"default_stone_block.png",
		"default_stone_block.png^mobs_zombie_spawner.png", -- Front
		"default_stone_block.png^mobs_zombie_spawner.png", -- Back
	},
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.375, 0.375, -0.3125, 0.375}, -- NodeBox1
			{-0.3125, -0.5, -0.3125, 0.3125, -0.25, 0.3125}, -- NodeBox2
			{-0.25, -0.4375, -0.125, 0.25, 0.1875, 0.125}, -- NodeBox4
			{-0.1875, -0.4375, -0.125, 0.1875, 0.25, 0.125}, -- NodeBox3
		}
	}
})

	
