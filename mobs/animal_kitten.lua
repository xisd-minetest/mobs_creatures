-- Adapted from Kitten by Jordach / BFD
--

-- Intllib
local S = mobs.intllib

-- Mod related infos
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

--
-- Documentation for help/doc modpack
--
local longdesc, usagehelp
longdesc = ''
longdesc = longdesc..'\n'..mobs.textstrings.longdesc.peacefull_land
longdesc = longdesc..'\n'..S(".")
usagehelp = ''
local food = S("fish")
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.feed_with(food)
usagehelp = usagehelp..'\n'..mobs.textstrings.usagehelp.tamed_catch_net_or_lasso

local mob_id = modname..":kitten"

mobs:register_mob(mob_id, {
	type = "animal",
	passive = true,
	hp_min = 5,
	hp_max = 10,
	armor = 200,
	collisionbox = {-0.3, -0.3, -0.3, 0.3, 0.1, 0.3},
	visual = "mesh",
	visual_size = {x = 0.5, y = 0.5},
	mesh = "mobs_kitten.b3d",
	textures = {
		{"mobs_kitten_striped.png"},
		{"mobs_kitten_splotchy.png"},
		{"mobs_kitten_ginger.png"},
		{"mobs_kitten_sandy.png"},
	},
	makes_footstep_sound = false,
	sounds = {
		random = "mobs_kitten",
	},
	walk_velocity = 0.6,
	run_velocity = 2,
	runaway = true,
	jump = false,
	drops = {
		{name = "farming:string", chance = 1, min = 1, max = 1},
	},
	water_damage = 1,
	lava_damage = 5,
	fear_height = 3,
	animation = {
		speed_normal = 42,
		stand_start = 97,
		stand_end = 192,
		walk_start = 0,
		walk_end = 96,
	},
	follow = {"ethereal:fish_raw", "mobs_fish:clownfish", "mobs_fish:tropical",
				modname..":clownfish", modname..":fish_tropical",
				"mobs:clownfish", "mobs:fish_tropical",
				"mobs_animal:rat", modname..":rat", "mobs:rat"},
	view_range = 8,
	on_rightclick = function(self, clicker)

		if mobs:feed_tame(self, clicker, 4, true, true) then return end
		if mobs:protect(self, clicker) then return end
		if mobs:capture_mob(self, clicker, 30, 40, 40, false, nil) then return end
	end
})


mobs:spawn({
	name = mob_id,
	nodes = {"default:dirt_with_grass", "ethereal:grove_dirt"},
	min_light = 12,
	interval = 900, --30
	chance = 72000,
	min_height = 45,
	max_height = 31000,
	day_toggle = true,
})


mobs:register_egg(mob_id, S("Kitten"), "mobs_kitten_inv.png", 0)
mobs:doc_identifier_compat(mob_id, longdesc, usagehelp)


mobs:alias_mob("mobs:kitten", mob_id) -- compatibility
